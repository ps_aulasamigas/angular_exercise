import { Routes, RouterModule }	from '@angular/router';
import { BookingComponent }		  from './components/booking/booking.component';
import { AppComponent }		      from './app.component';

const appRoutes: Routes = [
  {
    path: '',
    component: BookingComponent
  },
  {
    path: 'booking',
    component: BookingComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes, { useHash: true });