import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userSearch'
})

export class UserPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) return value;

    return value.filter((item) => item.firstName.startsWith(args));
  }

}
