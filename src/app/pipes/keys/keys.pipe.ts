import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keys'
})
export class KeysPipe implements PipeTransform {

  options = [];

  transform(value, args:string[]) : any {
    if (this.options.length == 0 )
    {
      for (var enumMember in value) {
        if (parseInt(enumMember) == Number(enumMember)) {
          this.options.push({key: enumMember, value: value[enumMember]});
        }
      }
    }
    return this.options;
  }

}
