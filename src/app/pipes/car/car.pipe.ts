import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'carSearch'
})
export class CarPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) return value;

    return value.filter((item) => item.brand.startsWith(args));
  }

}
