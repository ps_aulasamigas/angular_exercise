import { Component, OnInit, ViewChild  }  from '@angular/core';
import { ModalComponent }                 from 'ng2-bs3-modal/ng2-bs3-modal';
import { User }                           from '../../models/user';
import { UserService }                    from '../../services/user/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @ViewChild('modalUser')
  modal: ModalComponent;
  user: User = new User();
  errorMessage: string;
  users: User[];

  constructor(
    private userService: UserService) { }

  ngOnInit() { }

  async addUser() {
    let response = await this.userService.addUser(this.user)
                            .then((data) => {
                              this.user = <User>data;
                            })
                            .catch((error) => {
                              this.errorMessage = <any>error
                            });

    if (typeof this.user.id !== "undefined")
    {
      alert('Agregaste el usuario con exito.');
      this.user = new User();
      this.modal.close();
    }
  }

  clearForm() {
    this.user = new User();
  }
}
