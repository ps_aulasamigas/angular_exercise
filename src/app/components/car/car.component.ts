import { Component, OnInit, ViewChild }   from '@angular/core';
import { ModalComponent }                 from 'ng2-bs3-modal/ng2-bs3-modal';
import { Car }                            from '../../models/car';
import { CarService }                     from '../../services/car/car.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  @ViewChild('modalCar')
  modal: ModalComponent;
  car: Car = new Car();
  errorMessage: string;
  cars: Car[];

  constructor(
    private carService: CarService) { }

  ngOnInit() { }

  async addCar() {
    let response = await this.carService.addCar(this.car)
                    .then((data) => {
                      this.car = <Car>data;
                    })
                    .catch((error) => {
                      this.errorMessage = <any>error
                    });

    if (typeof this.car.id !== "undefined")
    {
      alert('Agregaste el carro con exito.');
      this.car = new Car();
      this.modal.close();
    }
  }

  clearForm() {
    this.car = new Car();
  }
}
