import { Component, OnInit }	from '@angular/core';
import { Router } 				    from '@angular/router';
import { Booking }            from '../../models/booking';
import { User }               from '../../models/user';
import { Car }                from '../../models/car';
import { BookingService }     from '../../services/booking/booking.service';
import { UserService }        from '../../services/user/user.service';
import { CarService }         from '../../services/car/car.service';
import { UserPipe }           from '../../pipes/user/user.pipe';
import { CarPipe }            from '../../pipes/car/car.pipe';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
  providers: [ BookingService, UserService, CarService, UserPipe, CarPipe ]
})

export class BookingComponent implements OnInit {

  booking: Booking = new Booking();
  filters = {
    user:'',
    car:''
  };
  errorMessage: string;
  users: User[];
  cars: Car[];
  bookings: Booking[];

  constructor(
    private bookingService: BookingService,
    private userService: UserService,
    private carService: CarService) {
    this.bookings = [];
  }

  ngOnInit() {
    this.getUsers();
    this.getCars();
  }

  async getUsers() {
    let response = await this.userService.getUsers()
                        .then((data) => {
                          this.users = <User[]>data;
                        })
                        .catch((error) => {
                          this.errorMessage = <any>error
                        });
  }

  async getCars() {
    let response = await this.carService.getCars()
                        .then((data) => {
                          this.cars = <Car[]>data;
                        })
                        .catch((error) => {
                          this.errorMessage = <any>error
                        });
  }

  async addBooking() {
    let response = await this.bookingService.addBooking(this.booking)
                            .then((data) => {
                              this.booking = <Booking>data;
                            })
                            .catch((error) => {
                              this.errorMessage = <any>error
                            });

    if (typeof this.booking.id !== "undefined")
    {
      alert('Agregaste la reserva con exito.');
      this.bookings.push(this.booking);
      this.booking = new Booking();
    }
  }
}
