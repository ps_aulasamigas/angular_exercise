import { BrowserModule }	    from '@angular/platform-browser';
import { NgModule }			      from '@angular/core';
import { FormsModule } 		    from '@angular/forms';
import { HttpModule } 		    from '@angular/http';
import { Ng2Bs3ModalModule }  from 'ng2-bs3-modal/ng2-bs3-modal';

import { AppComponent } 	    from './app.component';
import { routing }       	    from './app.routing';

import { BookingComponent }   from './components/booking/booking.component';
import { UserComponent }      from './components/user/user.component';
import { CarComponent }       from './components/car/car.component';
import { UserDirective }      from './directives/user/user.directive';
import { CarDirective }       from './directives/car/car.directive';
import { UserPipe }           from './pipes/user/user.pipe';
import { CarPipe }            from './pipes/car/car.pipe';
import { KeysPipe }           from './pipes/keys/keys.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BookingComponent,
    UserComponent,
    CarComponent,
    UserDirective,
    CarDirective,
    UserPipe,
    CarPipe,
    KeysPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
	  routing,
    Ng2Bs3ModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
