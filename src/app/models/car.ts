export enum TypeCar {
	Turismo,
	Deportivo,
	Monovolumen,
	Todoterreno,
	Furgoneta,
	Camioneta
}

export class Car {
	constructor(
		public id?: string,
		public brand?: string,
		public plate?: string,
		public type = TypeCar) {}
}