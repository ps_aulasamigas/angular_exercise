export class Booking {
    constructor(
        public id?: number,
        public userId?: number,
        public carId?: number,
        public startDate?: Date,
        public endDate?: Date,
        public detail?: string) {}
}