import { Injectable }               from '@angular/core';
import { Http, Response }           from '@angular/http';
import { Headers, RequestOptions }  from '@angular/http';
import { User }                     from '../../models/user';
import { Constants }                from '../../constants/constants';
import { Base }                     from '../../commons/base';

@Injectable()
export class UserService extends Base {

  constructor (private http: Http) {
    super();
  }

  async getUsers(): Promise<User[]> {
    return this.http.get(Constants.USER_DATA)
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
  }

  async addUser(user: User): Promise<User> {
    let body    = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(Constants.USER_DATA, body, options)
                    .toPromise()
                    .then(res => <User> res.json())
                    .catch(this.handleError);
  }

}
