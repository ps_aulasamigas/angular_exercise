import { Injectable }               from '@angular/core';
import { Http, Response }           from '@angular/http';
import { Headers, RequestOptions }  from '@angular/http';
import { Car }                      from '../../models/car';
import { Constants }                from '../../constants/constants';
import { Base }                     from '../../commons/base';

@Injectable()
export class CarService extends Base {

  constructor (private http: Http) {
    super();
  }

  async getCars(): Promise<Car[]> {
    return this.http.get(Constants.CAR_DATA)
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
  }

  async addCar(car: Car): Promise<Car> {
    let body    = JSON.stringify(car);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(Constants.CAR_DATA, body, options)
                    .toPromise()
                    .then(res => res.json())
                    .catch(this.handleError);
  }

}
