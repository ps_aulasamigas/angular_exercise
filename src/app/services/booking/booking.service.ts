import { Injectable }               from '@angular/core';
import { Http, Response }           from '@angular/http';
import { Headers, RequestOptions }  from '@angular/http';
import { Booking }                  from '../../models/booking';
import { User }                     from '../../models/user';
import { Car }                      from '../../models/car';
import { Constants }                from '../../constants/constants';
import { Base }                     from '../../commons/base';

@Injectable()
export class BookingService extends Base {

  constructor (private http: Http) {
    super();
  }

  getBookings(): Promise<Booking[]>  {
    return this.http.get(Constants.BOOKING_DATA)
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
  }

  async addBooking(booking: Booking): Promise<Booking> {
    let body    = JSON.stringify(booking);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(Constants.BOOKING_DATA, body, options)
                    .toPromise()
                    .then(res => <Booking>res.json())
                    .catch(this.handleError);
  }

}
