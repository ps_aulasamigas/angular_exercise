export class Constants {
    static CONNECTION_STRING: string    = "mongodb://localhost:27017/angular_exercise";
    static USERNAME: string             = "angular2";
    static PASSWORD: string             = "ngExercise";
    static USER_DATA: string            = "http://localhost:8000/api/user";
    static CAR_DATA: string             = "http://localhost:8000/api/car";
    static BOOKING_DATA: string         = "http://localhost:8000/api/booking";
}