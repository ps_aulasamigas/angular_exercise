(function($) {
    $(function() {
        $('#startDate').datepicker(
            {
                format: 'yyyy-m-d',
                language: 'es'
            }
        );

        $('#endDate').datepicker(
            {
                format: 'yyyy-m-d',
                language: 'es'
            }
        );

        $('#btn_icon-plan').on('click', function(evt) {
            if($(this).hasClass('add-planCurricular'))
            {
                $('body').css({
                    'overflow':'hidden'
                });

                $(this).addClass('close-planCurricular')
                    .removeClass('add-planCurricular')
                    .next().fadeIn()
                    .next().fadeIn();
            }
            else
            {
                $('body').removeAttr('style');

                $(this).addClass('add-planCurricular')
                    .removeClass('close-planCurricular')
                    .next().fadeOut()
                    .next().fadeOut();
            }
            evt.stopPropagation();
        });
    });
})(jQuery);