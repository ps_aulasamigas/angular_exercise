import { AngularExercisePage } from './app.po';

describe('angular-exercise App', function() {
  let page: AngularExercisePage;

  beforeEach(() => {
    page = new AngularExercisePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
